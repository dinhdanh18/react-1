import React, { Component } from 'react'
import "../Style/style.css"

export default class Banner extends Component {
  render() {
    return (
      <div className='banner'>
        <h1 >a warm welcome</h1>
        <p className='my-4'>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odio ut quisquam reiciendis excepturi sint veniam laudantium nisi quia animi optio?</p>
        <button className='btn btn-primary'>Call to Action</button>
      </div>
    )
  }
}
