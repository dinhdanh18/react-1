import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className='footer'>
        <p>CopyRight @ Your Website 2019</p>
      </div>
    )
  }
}
