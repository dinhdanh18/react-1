import logo from './logo.svg';
import './App.css';
import NavBar from './Component/NavBar';
import Banner from './Component/Banner';
import Item from './Component/Item';
import "./Style/style.css"
import Footer from './Component/Footer';
function App() {
  return (
    <div>
        <NavBar/>
        <div className="container">
          <Banner/>
          <div className='item'>
            <Item/>
            <Item/>
            <Item/>
            <Item/>
          </div>
        </div>
        <Footer/>
    </div>
  );
}

export default App;
